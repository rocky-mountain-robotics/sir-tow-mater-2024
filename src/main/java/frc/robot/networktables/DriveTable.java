package frc.robot.networktables;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.networktables.DoubleArrayPublisher;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;

public class DriveTable {

  private NetworkTable driveNetworkTable;

  private DoubleArrayPublisher posePublisher;

  public DriveTable() {
    driveNetworkTable = NetworkTableInstance.getDefault().getTable("DriveTrain");

    posePublisher = driveNetworkTable.getDoubleArrayTopic("Pose").publish();
  }

  public void setPose(Pose2d pose) {
    posePublisher.set(new double[] {
      pose.getX(),
      pose.getY(),
      pose.getRotation().getRadians()});
  }

  public void close() {
    posePublisher.close();
  }

}
