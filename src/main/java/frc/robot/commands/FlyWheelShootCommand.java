// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.FlyWheelSubsystem;

public class FlyWheelShootCommand extends Command {
  /** Creates a new FlyWheelShootCommand. */
  FlyWheelSubsystem robotFlyWheel;

  public FlyWheelShootCommand(FlyWheelSubsystem robotFlyWheel) {
    // Use addRequirements() here to declare subsystem dependencies.
    this.robotFlyWheel = robotFlyWheel;
    addRequirements(robotFlyWheel);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    // Need set velocity
    robotFlyWheel.spinFlyWheelPower(.75);

  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
