// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.WormHoleSubsystem;

public class SetServoAngleCommand extends Command {

  private WormHoleSubsystem servo;
  private Double angle;

  /** Creates a new SetServoAngle. */
  public SetServoAngleCommand(Double angleInDegrees, WormHoleSubsystem RobotServo) {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(RobotServo);
    servo = RobotServo;
    angle = angleInDegrees;
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    servo.setAngle(angle);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
