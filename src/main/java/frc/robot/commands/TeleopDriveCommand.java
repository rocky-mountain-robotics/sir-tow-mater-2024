// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.button.CommandJoystick;
import frc.robot.Constants.FestiveBaton;
import frc.robot.Constants.OIConstants;
import frc.robot.subsystems.DriveSubsystem;

public class TeleopDriveCommand extends Command {
  private final DriveSubsystem m_robotDrive;
  private final CommandJoystick m_driverController = new CommandJoystick(OIConstants.kDriverControllerPort);

  /** Creates a new TeleopDriveCommand. */
  public TeleopDriveCommand(DriveSubsystem robotDrive) {
    // Use addRequirements() here to declare subsystem dependencies.
    m_robotDrive = robotDrive;
    addRequirements(robotDrive);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
   
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    m_robotDrive.drive(
      -MathUtil.applyDeadband(m_driverController.getY(),     OIConstants.kDriveDeadband),
      -MathUtil.applyDeadband(m_driverController.getX(),     OIConstants.kDriveDeadband),
      -MathUtil.applyDeadband(m_driverController.getTwist(), OIConstants.kTwistDeadband),
      true, true
    );
    m_driverController.button(FestiveBaton.kBaseButton8).onTrue(new InstantCommand(() -> m_robotDrive.zeroHeading()));
    m_driverController.button(FestiveBaton.kTopButton3).onTrue(new InstantCommand(() -> m_robotDrive.setX()));
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
