// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkLowLevel.MotorType;
import edu.wpi.first.networktables.DoublePublisher;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.IntakeCanIds;


/** Add your docs here. */
public class IntakeSubsytem extends SubsystemBase{

    // final DoubleSubscriber shuffleboardDoubleSubscriber;
    final DoublePublisher intakeEncoderCounts;


    private CANSparkMax intakeMotor;
    private NetworkTable intakeTable;

    public IntakeSubsytem() {

        intakeMotor = new CANSparkMax(IntakeCanIds.kintakeMotorCanId, MotorType.kBrushless);
        intakeTable = NetworkTableInstance.getDefault().getTable("Intake");
        intakeEncoderCounts = intakeTable.getDoubleTopic("Encoder Counts").publish();

    }

    // @Override
    // private void periodic() {
    //     intakeEncoderCounts.set(fill out here w/ updater);
    // }

    public void runIntakeMotor(double power) {
        intakeMotor.set(power);
    }

}