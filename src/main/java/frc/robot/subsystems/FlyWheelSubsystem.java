// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;
import com.revrobotics.CANSparkLowLevel.MotorType;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.controller.SimpleMotorFeedforward;
import edu.wpi.first.networktables.DoublePublisher;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.FlyWheelConstants;

public class FlyWheelSubsystem extends SubsystemBase {
  /** Creates a new FlyWheelSubsystem. */

  private CANSparkMax robotFlyWheel;

  private RelativeEncoder flyWheelEncoder;

  private SimpleMotorFeedforward flyWheelFeedForward;

  private NetworkTable flyWheelTable;

  private DoublePublisher flyWheelEncoderValues;

  private PIDController flyWheelPID;

  public FlyWheelSubsystem() {

    robotFlyWheel = new CANSparkMax(FlyWheelConstants.FlyWheelCANId, MotorType.kBrushless);

    flyWheelEncoder = robotFlyWheel.getEncoder();

    // Need to create/tune constants for feed forward -- Wyatt
    flyWheelFeedForward = new SimpleMotorFeedforward(0, 0);

    flyWheelPID = new PIDController(0, 0, 0);

    flyWheelTable = NetworkTableInstance.getDefault().getTable("flyWheelTable");

    flyWheelEncoderValues = flyWheelTable.getDoubleTopic("flyWheelVelocity").publish();

  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    flyWheelEncoderValues.set(flyWheelEncoder.getVelocity());
  }

  public void spinFlyWheelPower(double power) {
    robotFlyWheel.set(power);
  }

  public void spinFlyWheelFeedForwardPID(double setVelocity) {
    robotFlyWheel.setVoltage(
        flyWheelFeedForward.calculate(setVelocity) + flyWheelPID.calculate(flyWheelEncoder.getVelocity(), setVelocity));

  }
}