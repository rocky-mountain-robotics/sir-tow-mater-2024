// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

//Put here in case intellicense doesn't automatically add-Wyatt
import edu.wpi.first.networktables.DoubleSubscriber;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.Servo;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

/** Add your docs here. */
public class WormHoleSubsystem extends SubsystemBase{

  public Servo wormholeServo;

  public WormHoleSubsystem() {
    //Change channel if needed
  wormholeServo = new Servo(0);
  }

  public void up() {
    wormholeServo.setPosition(1);
  }

  public void hold() {
    wormholeServo.setPosition(0.5);
  }

  public void down() {
    wormholeServo.setPosition(0);
  }

  public void setAngle(double angleInDegrees){

    //Converts degrees to servo position then sets the position
    double angle = angleInDegrees;
    angle = Math.toRadians(angle) / Math.PI;
    wormholeServo.setPosition(angle);
  }

}
