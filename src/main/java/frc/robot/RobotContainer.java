// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.button.CommandJoystick;
import edu.wpi.first.wpilibj2.command.button.CommandXboxController;
import edu.wpi.first.wpilibj2.command.button.Trigger;
import frc.robot.Constants.FestiveBaton;
import frc.robot.Constants.OIConstants;
import frc.robot.commands.Autos;
import frc.robot.commands.SetServoAngleCommand;
import frc.robot.commands.TeleopDriveCommand;
import frc.robot.subsystems.DriveSubsystem;
import frc.robot.subsystems.FlyWheelSubsystem;
import frc.robot.subsystems.WormHoleSubsystem;

/**
 * This class is where the bulk of the robot should be declared. Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of the robot (including
 * subsystems, commands, and trigger mappings) should be declared here.
 */
public class RobotContainer {
  // The robot's subsystems and commands are defined here...
  private final DriveSubsystem m_robotDrive = new DriveSubsystem();
  private final CommandJoystick m_driverController = new CommandJoystick(OIConstants.kDriverControllerPort);
  private final CommandJoystick m_manipulatorController = new CommandJoystick(OIConstants.kManipulatorControllerPort);
  private final WormHoleSubsystem wormhole = new WormHoleSubsystem();
  private final FlyWheelSubsystem m_flyWheelSubsystem = new FlyWheelSubsystem();
  /** The container for the robot. Contains subsystems, OI devices, and commands. */
  public RobotContainer() {
    // Configure the trigger bindings
    configureBindings();
    m_robotDrive.setDefaultCommand(new TeleopDriveCommand(m_robotDrive));
  }

  /**
   * Use this method to define your trigger->command mappings. Triggers can be created via the
   * {@link Trigger#Trigger(java.util.function.BooleanSupplier)} constructor with an arbitrary
   * predicate, or via the named factories in {@link
   * edu.wpi.first.wpilibj2.command.button.CommandGenericHID}'s subclasses for {@link
   * CommandXboxController Xbox}/{@link edu.wpi.first.wpilibj2.command.button.CommandPS4Controller
   * PS4} controllers or {@link edu.wpi.first.wpilibj2.command.button.CommandJoystick Flight
   * joysticks}.
   */
  private void configureBindings() {
    // All manipulator controller button stuff.

    // Servo Control
    m_manipulatorController.button(FestiveBaton.kTopButton6).onTrue(new SetServoAngleCommand(90.0, wormhole));
    m_manipulatorController.button(FestiveBaton.kTopButton4).onTrue(new SetServoAngleCommand(180.0, wormhole));
    m_manipulatorController.button(FestiveBaton.kBaseButton10).onTrue(new SetServoAngleCommand(0.0, wormhole));

    // // Climber Control
    // m_manipulatorController.button(FestiveBaton.kTopButton5).whileTrue(Fill with raise command);
    // m_manipulatorController.button(FestiveBaton.kTopButton3).whileTrue(Fill with lower command);

    // // Flywheel Spin up
    // m_manipulatorController.button(FestiveBaton.kTriggerButton).whileTrue(Flywheel Command goes here);

    // All driver controller button stuff.

    // // Intake control
    // m_driverController.button(FestiveBaton.kThumbButton).whileTrue(Intake command go here);
  }

  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  public Command getAutonomousCommand() {
    // An example command will be run in autonomous
    return Autos.exampleAuto(m_robotDrive);
  }
}
