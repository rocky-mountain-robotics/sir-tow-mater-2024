# Acronyms and names:

+ Festive Baton: Joystick
+ PTAAS: Pneumatic Torsal Attire Accelerator Structure - T-shirt Cannon
  - Hasten Torsal Attire Unit - Fire cannon
+ PABA: Photon Absorbing Blinding Apparatus - Limelight Camera
+ AMP: Altitude Manipulator of Pulling - Climbing Apparatus
- Third Tow of the Month - Lowers arm
- Get 'er Done 95 - Raises arm

+ Controller Mapout
  - Manipulator controller
    - 3-6 used
    - 5 (up), and 3 (down) Climber commands
    - servo 6 (raise/set90), and 4 (commit to sender (flywheels))
    - Trigger Flywheel spin up 
    <!-- - Or Thumb Button? -->
    - 10 resets servo
    - Driver Controller
    - motion axes for driving
    - thumb button--run intake
    - button 11 sequence for lining up, and scoring
    - reset orientation 7
  - Approved by official Drivers and Manipulators, and Programmers

  + TENT - Theoretically Executed, Not Tested